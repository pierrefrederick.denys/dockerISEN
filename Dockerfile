FROM php:5.6-apache
RUN apt-get update && apt-get install -y libpq-dev && docker-php-ext-install pdo pdo_pgsql
COPY api/ /var/www/html/ 
RUN chown -R www-data:www-data /var/www/html 
RUN chmod -R 755 /var/www/html 
