<?php
require_once 'identifiants.php'; // fichier des identifiants BDD


function connexionDB() //Fonction permettant de se connecter à une base de données
{
    try
    {
        $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        $BDD = new PDO(ADRESSE_BDD, LOGIN_BDD, PASS_BDD, $options);

    }
    catch (PDOException $e)
    {
        throw new Exception($e->getMessage(), $e->getCode());
    }
    return $BDD;
}



function createDatabaseIfNotExists(PDO $BDD)
{
    $request = "CREATE TABLE IF NOT EXISTS wheather2docker (
	id                   SERIAL ,
	date_measure         TIMESTAMP  NOT NULL ,
	location             VARCHAR (255) NOT NULL ,
	temp_c               VARCHAR (255) NOT NULL ,
	temp_f               VARCHAR (255) NOT NULL ,
	humidity             VARCHAR (255) NOT NULL ,
	wind_dir             VARCHAR (255) NOT NULL ,
	wind_kph             VARCHAR (255) NOT NULL ,
	uv                   VARCHAR (255) NOT NULL ,
	precipitation        VARCHAR (255) NOT NULL ,
	icon                 VARCHAR (255) NOT NULL
);";
    $BDD->query($request);
}



function getAllWeatherData(PDO $BDD){
    $request = $BDD->query('SELECT * FROM wheather2docker;');
    return $request->fetchAll(PDO::FETCH_ASSOC);
}


function setWeatherData(PDO $BDD, $date_measure, $location, $temp_c,$temp_f, $humidity, $wind_dir, $wind_kph, $uv, $precipitation, $icon){
    $request = $BDD->prepare('INSERT INTO wheather2docker(date_measure, location, temp_c, temp_f, humidity, wind_dir, wind_kph, uv, precipitation, icon) VALUES (:date_measure, :location, :temp_c, :temp_f, :humidity, :wind_dir, :wind_kph, :uv, :precipitation, :icon)');
    $request->execute(array('date_measure' => $date_measure, 'location' => $location, 'temp_c' => $temp_c, 'temp_f' => $temp_f, 'humidity' => $humidity, 'wind_dir' => $wind_dir, 'wind_kph' => $wind_kph, 'uv'=> $uv, 'precipitation'=>$precipitation, 'icon' => $icon ));
}
