<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/functions.php';

$json_string = file_get_contents("http://api.wunderground.com/api/5c4a5e8fb2dd8ab8/geolookup/conditions/forecast/q/Australia/Sydney.json");
$parsed_json = json_decode($json_string, true);
//var_dump($parsed_json);
$location = $parsed_json['location']['city'];

$forecasts = $parsed_json['forecast']['txt_forecast']['forecastday'];

$date_measure = $parsed_json['forecast']['txt_forecast']['date'];
$t=time();
$date_measure  = date("Y-m-d h:i:sa", $t);
$temp_c = $parsed_json['current_observation']['temp_c'];
$temp_f = $parsed_json['current_observation']['temp_f'];
$humidity = $parsed_json['current_observation']['relative_humidity'];
$wind_dir = $parsed_json['current_observation']['wind_dir'];
$wind_kph = $parsed_json['current_observation']['wind_kph'];
$uv = $parsed_json['current_observation']['UV'];
$precipitation = $parsed_json['current_observation']['precip_1hr_in'];
$icon = $parsed_json['current_observation']['icon_url'];

if (isset($temp_f) && isset($forecasts)) {

    try {
        $bdd = connexionDB();
        if (isset($bdd)) {
            createDatabaseIfNotExists($bdd);

            setWeatherData($bdd, $date_measure, $location, $temp_c, $temp_f, $humidity, $wind_dir, $wind_kph, $uv, $precipitation, $icon);

        } else {
            echo '<div class="alert alert-danger" role="alert"><i class="fa fa-times-circle"></i> Erreur de connexion à la base de donnée</div>';
        }
        echo "Elément ajouté !";
        $bdd = null;
    } catch (PDOException $e) {
        print "Erreur de base de données : " . $e->getMessage() . "<br/>";
        die();
    }

} else {
    echo "Erreur d'API !";
}

?>